#include <iostream>

using namespace std;

const int tamanho = 10;

int i;
int maior;
int menor;
int vetor;
int valor;

struct pontos {
    int x;
    int y;
};

struct pontos p[tamanho];

void incrementarPontos (int tam){
    for(i=0; i < tam; i++){
    cout << "INSIRA O VALOR DE X NA POSICAO " << (i+1) << ": " << endl;
    cin >> p[i].x;
    }
    cout << "-------------------" << endl;

    for(i=0; i < tam; i++){
    cout << "INSIRA O VALOR DE Y NA POSICAO " << (i+1) << ": " << endl;
    cin >> p[i].y;
    }
}

void maiorValordeX(pontos p[], int tam){
    for(i=0; i<tam; i++){
        if (p[i].x > maior){
            maior = p[i].x;
        }
    }
   cout << "MAIOR VALOR DE X: " << maior << endl;
}

void menorValorY(pontos p[], int tam){
    for(i=0; i<tam; i++){
        if (p[i].y < maior){
            menor = p[i].y;
        }
    }
   cout << "MAIOR VALOR DE Y: " << menor << endl;
}

void procurarValoremXeY(pontos p[], int tam){
    cout << "INSIRA O VALOR A SER PROCURADO NOS VETORES X e Y:" << endl;
    cin >> valor;
    cout << endl;
    for(i=0; i<tam; i++){
        if(valor == p[i].x){
           cout << "POSICOES QUE CONTEM O VETOR X:" << i << endl;
        }
    }
    for(i=0; i<tam; i++){
        if(valor == p[i].y){
            cout << "POSICOES QUE CONTEM O VETOR Y:" << i << endl;
        }
    }
}
int main()
{
    cout <<"***ESTE PROGRAMA APRESENTA O MAIOR E O MENOR VALOR ENTRE X E Y***\n\n"<< endl;
    incrementarPontos(tamanho);
    procurarValoremXeY(p, tamanho);
    maiorValordeX(p, tamanho);
    menorValorY(p, tamanho);
    return 0;
}
